
const admin = require('firebase-admin');
var Gpio = require('onoff').Gpio;

var serviceAccount = require('./consumerelectronics-572f6-firebase-adminsdk-ua4e2-520c094d2c.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

var db = admin.firestore();

db.collection('home_devices').get()
.then((snapshot) => {
    snapshot.forEach((doc) => {
        console.log(doc.id, '=>', doc.data());
        doc.ref.onSnapshot((sh) => onDeviceChanged(sh));
    });
})
.catch((err) => {
    console.log('Error getting documents', err);
});

function onDeviceChanged(device) {
    var LED =  new Gpio(device.get('pin'), 'out')
    if(device.get('is_on')== true)
        LED.writeSync(1)
    else
        LED.writeSync(0)
    console.log(device.id,device.data());
}
