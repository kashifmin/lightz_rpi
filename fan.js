var setInterval = require('timers').setInterval;

var Gpio = require('onoff').Gpio;
const admin = require('firebase-admin');

var serviceAccount = require('./consumerelectronics-572f6-firebase-adminsdk-ua4e2-520c094d2c.json');

var outputPin =  new Gpio(18, 'out');

const SPEED_STEPS = 5;
// large TIMING for testing with LED. Must be changed to 10ms in real time.
const PULSE_TIMING = 5000; // gap between each input pulse in ms
const PULSE_WIDTH = 500;

var fanSpeed = 1;

function getDelayForSpeed() {
    return (PULSE_TIMING / SPEED_STEPS) * fanSpeed;
}

// util for creating Promsie by delay
function delayedPromise(delayms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, delayms);
    });
}

async function generateInputPulses() {
    // for now we mock the input signal/clock using setInterval
    // TODO: replace with input from a GPIO pin
    setInterval(onInputPulse, PULSE_TIMING);
}

// generateInputPulses must  call this whenever there's an input pulse
// probably from a GPIO input
async function onInputPulse()  {
    console.log("INPUT PULSE");
    // execute something after estimated ms
    delayedPromise(getDelayForSpeed()).then(() => {
        generateOutputPulse();
    });
}

// generates an output pulse with the defined PULSE_WIDTH
async function generateOutputPulse() {
    console.log("OUTPUT PULSE");
    outputPin.writeSync(1);
    await delayedPromise(PULSE_WIDTH).then(() => outputPin.writeSync(0));
}

// init firebase stuff
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

var db = admin.firestore();

db.collection('fans').get()
.then((snapshot) => {
    snapshot.forEach((doc) => {
        console.log(doc.id, '=>', doc.data());
        doc.ref.onSnapshot((sh) => {
            console.log("Fan speed changed: ", sh.data());
            fanSpeed = sh.data().speed
        });
    });
})
.catch((err) => {
    console.log('Error getting documents', err);
});

// clear output pin state and wait for input pulses
outputPin.writeSync(0);
generateInputPulses();

